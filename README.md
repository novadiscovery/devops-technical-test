# DevOps Technical Test

You are provided with a Kubernetes config file and AWS access keys. You have the following access:

- Kubernetes cluster: admin
- AWS `eu-central-1` (Frankfurt) region:
  - EKS: read-only on K8S cluster `jinko-dev-2-bis`
  - S3: read/write on S3 buckets `devops-tech-test*`
  - Route53: read/write on Hosted Zone `recruitment.dev.jinko.ai`
  - CloudFormation: (read/write only stacks named `recruitment-*`)

---

# Goal

Your goal is to deploy the [Novimg](https://gitlab.com/novadiscovery/novimg) application on Kubernetes and AWS. This application simply allow you to upload files to an S3 bucket.

## Requirements & constraints

- The app run with Docker image `registry.gitlab.com/novadiscovery/novimg/server:master` (available publicly, no auth required).
- The app listen on port `8080` by default
- The app is configured through a json configuration file which must be mounted as `/home/node/app/config/local.json` in the container.
  - Example `config.json`:
    ```json
    {
      "storage": {
        "s3": true,
        "bucket": "devops-tech-test",
        "accessKeyId": "AKIAXXXXXXXXXXX",
        "secretAccessKey": "xxx"
      }
    }
    ```
  - **WARNING**: other files (including default config) are also present in `/home/node/app/config` and **MUST be kept at runtime**. This may require specific volume configuration.
- The app must be deployed on Kubernetes in namespace `devops-tech-test`
- The app must use S3 bucket `devops-tech-test` (property `bucket` in config)
- You may need to manage S3 bucket yourself

## Resources and advices

Use any method or tool you'll see fit.

You can use resources and templates in `resources/` which will provide you a Deployment and Secret template you can use to deploy the app. 
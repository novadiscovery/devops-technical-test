# Setup instructions

Instructions to setup the exercice

- Create an EKS Kubernetes cluster on AWS
- Run Ansible playbook on desired AWS account:
  ```
  ansible-playbook playbook.yml
  ```
